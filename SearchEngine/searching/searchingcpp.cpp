#include <iostream>
#include <string>
#include <map>
#include<sstream>
#include <vector>
#include <iterator>
#include <cmath>
#include <fstream>
#include <cstdlib>
#include <algorithm>
#include <string>
#include<sstream>
#include <map>
#include <vector>
#include <boost/serialization/map.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/serialization/map.hpp> 
#include <boost/serialization/string.hpp> 
#include <boost/serialization/list.hpp> 
#include <boost/serialization/vector.hpp> 
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

/*
Include Boost
*/

using namespace std;
//prototyping
void split(vector<string> &terms, string search);

//function which splits search term and stores it in vector
void split(vector<string> &terms, string search) {
	terms.clear();
	stringstream ss(search);
	string word;
	while (getline(ss, search, ' ')) {
		terms.push_back(word);
	}

}
int main() {
	map<string, vector<string>> FI;
	map<string, vector<string>> II;

	/*******************
	*
	*
	**  boost statements to deserialize maps go here ***
	*
	*
	********************/


	ifstream maps("forward.map", ios::binary);
	boost::archive::binary_iarchive oarch(maps);
	oarch >> FI;
	maps.close();
	ifstream maps2("inverted.map", ios::binary);
	boost::archive::binary_iarchive oarch1(maps2);
	oarch1 >> II;
	maps2.close();

	//Multi term Searching
	string search;
	cout << "Enter search terms: ";
	cin >> search;

	vector<string> terms; // holds search terms

						  //split terms and store them in vectors
	split(terms, search);

	//searching
	map<string, vector<int>> index; // holds words and their index in a particular document
	map<string, map<string, vector<int>>> Doc; // holds document and occurences of each term in it.
	string tempStr;
	string tempDoc;
	//creates a map with document title as key and search terms with their indexes in that document
	for (int i = 0; i < terms.size(); i++) {
		tempStr = terms[i];
		for (int j = 0; j < II[tempStr].size(); j++) {
			tempDoc = II[tempStr][j];
			int count = 0;//index
			for (int k = 0; k < FI[tempDoc].size(); k++) {
				if (FI[tempDoc][k].compare(tempStr) == 0) {
					index[tempStr].push_back(count);
				}
				count++;
			}
			Doc[tempDoc] = index;
		}
	}

	//printing the results
	for (map<string, map<string, vector<int>>>::iterator it = Doc.begin(); it != Doc.end(); it++) {
		cout << "Document Title: " << it->first << "\t" << "Search Term: ";
		for (map<string, vector<int>>::iterator it2 = it->second.begin(); it2 != it->second.end(); it2++) {
			cout << it2->first << " occurs at indices: ";
			for (int m = 0; m < it2->second.size(); m++) {
				cout << it2->second[m];
			}
			cout << "\n";
		}
		cout << "\n\n";
	}



	system("pause");
	return 0;
}