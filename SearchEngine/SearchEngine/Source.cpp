/* @file searching.cpp
Forward and Inverted Index generator for Simple wikipedia dataset
Abdullah Nasir     210715
Maooz Bin Zahid    226778
Hassan Kumail Ali  207674
Ghazi Tiwana       210563

*/
#include <iostream>
#include <cmath>
#include <fstream>
#include <cstdlib>
#include <algorithm>
#include <string>
#include<sstream>
#include <map>
#include <vector>
#include <boost/serialization/map.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/serialization/map.hpp> 
#include <boost/serialization/string.hpp> 
#include <boost/serialization/list.hpp> 
#include <boost/serialization/vector.hpp> 
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

using namespace std;
int main() {
	//forward indexing
	ios_base::sync_with_stdio(0);
	cin.tie(0);
	cout.tie(0);
	ifstream file;
	file.open("SimpleWiki.txt");

	map<string, vector<string>> fI;
	map<string, vector<string>> II;
	map<string, vector<string>>::iterator it1;


	if (!file.is_open()) {
		cout << "Error";
	}
	string line;
	string title;
	vector<string> words;

	getline(file, line);

	while (getline(file, line)) {
		if (line.compare("") == 0) {
			words.clear();
			getline(file, title); //decides title
		}
		else { //prints words included in file
			istringstream is(line);
			string token;
			while (getline(is, token, ' ')) {
				//removes punctuation from token
				for (int i = 0; i < token.size(); i++)
				{
					unsigned char c = token[i];
					if (ispunct(c))
					{
						token.erase(i, 1);
						i--;
					}
				}
				words.push_back(token);
			}

		}
		fI[title] = words;
	}
	file.close();

	//output file
	ofstream OF("ForwardIndexing.txt");
	int DocID = 0;

	//for (it1 = fI.begin(); it1 != fI.end(); it1++) {
	//int wordIndex = 0;
	//OF << "\n";
	//OF << "DocID " << DocID << "\n";
	//for (int i = 0; i < it1->second.size(); i++) {
	//OF << it1->second[i] << "[" << wordIndex << "]" << ",";
	//wordIndex++;
	//}
	//OF << "\n";
	//DocID++;
	//}

	OF.close();
	//Generates inverted index map from forward index map
	ofstream InvertedIndex("InvertedIndex.txt");
	for (it1 = fI.begin(); it1 != fI.end(); it1++) {
		for (int i = 0; i < it1->second.size(); i++) {
			II[it1->second[i]].push_back(it1->first);
		}
	}
	//***
	//writes inverted index to file

	//***

	InvertedIndex.close();
	ofstream maps("forward.map", ios::binary);
	boost::archive::binary_oarchive oarch(maps);
	oarch << fI;
	maps.close();
	ofstream maps2("inverted.map", ios::binary);
	boost::archive::binary_oarchive oarch1(maps2);
	oarch1 << II;
	maps2.close();

	return 0;
}