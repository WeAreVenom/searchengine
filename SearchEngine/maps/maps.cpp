#include <iostream>
#include <cmath>
#include <fstream>
#include <cstdlib>
#include <algorithm>
#include <string>
#include<sstream>
#include <map>
#include <vector>
#include <boost/serialization/map.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/serialization/map.hpp> 
#include <boost/serialization/string.hpp> 
#include <boost/serialization/list.hpp> 
#include <boost/serialization/vector.hpp> 
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
using namespace std;
int main() {
	map<string, vector<string>> fI;
	map<string, vector<string>> II;
	map<string, vector<string>>::iterator it1;

	ifstream maps("forward.map", ios::binary);
	boost::archive::binary_iarchive oarch(maps);
	oarch >> fI;
	maps.close();
	ifstream maps2("inverted.map", ios::binary);
	boost::archive::binary_iarchive oarch1(maps2);
	oarch1 >> II;
	maps2.close();

	ofstream InvertedIndex("invertedindex.txt");
	for (it1 = II.begin(); it1 != II.end(); it1++) {
		InvertedIndex << "\n";
		InvertedIndex << it1->first << "-" << "\n";
		for (int i = 0; i < it1->second.size(); i++) {
			InvertedIndex << it1->second[i] << ",";
		}
		InvertedIndex << "\n";
	}
	InvertedIndex.close();
}